all: site site/matilda.html site/lillian.html site/pink-triangles.png site/green-triangles.png

site:
	mkdir -p site

site/lillian.html: triangles.php
	NAME=Lillian BACKGROUND=pink-triangles.png COLOR=43e97d GA=UA-32700619-4 php triangles.php > site/lillian.html

site/matilda.html: triangles.php
	NAME=Matilda BACKGROUND=green-triangles.png COLOR=e94343 GA=UA-32700619-5 php triangles.php > site/matilda.html

site/pink-triangles.png: pink-triangles.svg
	inkscape pink-triangles.svg --export-png=site/pink-triangles.png

site/green-triangles.png: green-triangles.svg
	inkscape green-triangles.svg --export-png=site/green-triangles.png

clean:
	rm -f site/*
