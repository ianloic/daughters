<html>
  <head>
  <!--
  Colour palette:
  Light pink: #fce5e5;
  Dark pink: #e94343;
  Light green: #e6fcee;
  Dark green: #43e97d;
  Cream background: #fffffb;
  -->
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <style>
      body {
        background-image: url('<?= getenv("BACKGROUND") ?>');
        margin: 0;
      }

      h1 {
        margin-top: 50vh;
        position: relative;
        top: -50px;
        font-weight: normal;
        text-align: center;
        font-family: 'Dancing Script', cursive;
        font-size: 100px;
        //color: #43e97d;
        color: #<?= getenv("COLOR") ?>;
        text-shadow: 1px 1px 3px rgba(255,223,0, 0.5);
      }
    </style>
    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Person",
  "name": "<?= getenv("NAME") ?> McKellar",
  "gender": "http://schema.org/Female",
  "birthDate": "2015-07-17"
}
    </script>
  </head>
  <body>
    <h1><?= getenv("NAME") ?></h1>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', '<?= getenv("GA") ?>', 'auto');
      ga('send', 'pageview');
    </script>
  </body>
</html>
